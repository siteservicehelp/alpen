'use strict';

// Подключаем Gulp
var gulp = require("gulp");

// Подключаем плагины Gulp
var watch = require('gulp-watch'),
	autoprefixer = require('gulp-autoprefixer'), // Проставлет вендорные префиксы в CSS для поддержки старых браузеров
	uglify = require("gulp-uglify"), // Минимизация javascript
	sass = require('gulp-sass')(require('sass')), // переводит SASS в CSS	
    pug = require('gulp-pug'),
	sourcemaps = require('gulp-sourcemaps'),
	rigger = require('gulp-rigger'),  //объединяет html-файлы в один
	concat = require("gulp-concat"), // Объединение файлов - конкатенация
	rename = require("gulp-rename"), // Переименование файлов/
	cssmin = require('gulp-clean-css'), // Минимизация CSS    
    //cssnano = require("gulp-cssnano"), // Минимизация CSS    
    imagemin = require('gulp-imagemin'), // Сжатие изображение
    svgsprite = require('gulp-svg-sprite'), //Сборка svg-спрайта иконок
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload; //перезагрузка браузера

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        //php: 'build/',
        js: 'build/assets/js/',
        css: 'build/assets/css/',
        img: 'build/assets/images/',
        fonts: 'build/assets/fonts/'
    },
    src: { //Пути откуда брать исходники
        //html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        //php: 'src/*.php',
        js: 'src/js/**/*.js',//В стилях и скриптах нам понадобятся только main файлы
        style: 'src/sass/styles.scss',
        img: 'src/images/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*',
        pug: 'src/pug/*.pug'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        //html: 'src/**/*.html',
        //php: 'src/**/*.php',
        js: 'src/js/**/*.js',
        style: 'src/sass/**/*.scss',
        img: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*',
        pug: 'src/pug/**/*.pug'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 3000,
    logPrefix: "master"
};

/* --------------------------------------------------------
   ----------------- Таски ---------------------------
------------------------------------------------------------ */


/*gulp.task('html:build', async function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});*/

/*gulp.task('php', async function () {
    gulp.src(path.src.php) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.php)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});*/

gulp.task('pug', async function () {
    gulp.src(path.src.pug)
        .pipe(pug({pretty: '\t'}))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', async function () {
    gulp.src(path.src.js) //Найдем наш scripts файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('style:build', async function () {
    gulp.src(path.src.style) //Выберем наш styles.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(autoprefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true}));
});

gulp.task('image:build', async function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', async function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', gulp.series(
    //'html:build',
    //'php',
    'pug',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
));

gulp.task("watch", async function() {
  	gulp.watch([path.watch.style], gulp.series("style:build"));
  	gulp.watch([path.watch.js], gulp.series("js:build"));
  	//gulp.watch([path.watch.html], gulp.series("html:build"));
    //gulp.watch([path.watch.php], gulp.series("php"));
    gulp.watch([path.watch.pug], gulp.series("pug"));
  	gulp.watch([path.watch.img], gulp.series("image:build"));
  	gulp.watch([path.watch.fonts], gulp.series("fonts:build"));
});

gulp.task('webserver', async function() {
    browserSync(config);
});

gulp.task('clean', async function(cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', gulp.series('build', 'webserver', 'watch'));