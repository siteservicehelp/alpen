/*
 * Third party
 */


/*
 * Custom
 */
(function($) { $(function() {

    // Залипающее меню
    $(window).scroll(function(){
        if( $(window).scrollTop() > 100 ) {                
            $('header').addClass('header-sticky');                       
                                                                     
        } else {
            $('header').removeClass('header-sticky');              
        }
    });    

    // Кнопка Вверх
    $(window).scroll(function(){
        if ($(this).scrollTop() > 300) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    // ScrollTo - Скролл до якоря #     
    $('a.target-btn[href^="#"]').bind('click.smoothscroll',function (e) {
        e.preventDefault();
        var target = this.hash,
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - ($('header').height())
        }, 600);
        $('.menu-wrap').toggleClass('menu-show');
        $('.toggle-button').toggleClass('button-open');
    });

    // Инициализация карусели    
    $('.main-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 5000,
        arrows: false,
        fade: true,
        //prevArrow: '<img src="assets/images/prev-a.png" alt="" class="arr-prev">',
        //nextArrow: '<img src="assets/images/next-a.png" alt="" class="arr-next">',   
        dots: true,     
        speed: 1000
        /*responsive: [            
            {
                breakpoint: 1921,
                settings: {
                    centerPadding: '385px'
                }
            },
            {
                breakpoint: 1300,
                settings: {
                    centerPadding: '285px'
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    centerPadding: '185px'
                }
            },
            {
                breakpoint: 992,
                settings: {
                    centerPadding: '85px'
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    centerPadding: '25px'
                }
            }     
        ]*/
    });

    /*var tabCarousel = $('.tabs-carousel');
    tabCarousel.owlCarousel({
        items: 3,
        loop: false,  
        autoplay: false,
        dots: true,
        //center: true,
        margin: 30,
        nav: true,  
        navText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
        responsiveClass: true,
        responsive: {
            0:{
                items:1,
                nav:true,
                dots: true,
                margin: 0,
            },
            768: {
                items:2,
                nav:true,
                dots: true,
                margin: 20,
            },
            992: {
                items:2,
                nav:true,
                dots: false,
                margin: 30,
            },
            1200:{
                items:3,
                nav:true,
                dots: false,
                margin: 30,
            }
        }
    });*/

}) })(jQuery)